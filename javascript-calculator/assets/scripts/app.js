const defaultResult = 0
let currentResult = defaultResult
let logEntries = []

function getUserInput() {
    return parseInt(userInput.value)
}

function writeToLog(
    opeartionIdentifier, 
    prevResult, 
    operationNumber, 
    newResult
) {
    const logEntry = {
        operation: opeartionIdentifier,
        prevResult: prevResult,
        number: operationNumber,
        result: newResult
    }
    logEntries.push(logEntry)
    console.log(logEntries)
}

function calcResult(calcType) {

    const userInputGiven = getUserInput()

    // Check for valid calcTypes before running the rest of the function.
    if (calcType !== 'ADD' && calcType !== 'SUBTRACT' && calcType !== 'MULTIPLY' && calcType !== 'DIVIDE' || !userInputGiven) {
        return
    }

    const initialResult = currentResult

    if (calcType === 'ADD') {
        calcDescription = `${currentResult} + ${userInputGiven}`
        currentResult += userInputGiven
    } else if (calcType === 'SUBTRACT') {
        calcDescription = `${currentResult} - ${userInputGiven}`
        currentResult -= userInputGiven
    } else if (calcType === 'MULTIPLY') {
        calcDescription = `${currentResult} * ${userInputGiven}`
        currentResult *= userInputGiven
    } else if (calcType === 'DIVIDE') {
        calcDescription = `${currentResult} / ${userInputGiven}`
        currentResult /= userInputGiven
    }


    outputResult(currentResult, calcDescription)
    writeToLog(calcType, initialResult, userInputGiven, currentResult)
}

function add() {
    calcResult('ADD')
}

function subtract() {
    calcResult('SUBTRACT')
}

function multiply() {
    calcResult('MULTIPLY')
}

function divide() {
    calcResult('DIVIDE')
}


addBtn.addEventListener('click', add)
subtractBtn.addEventListener('click', subtract)
multiplyBtn.addEventListener('click', multiply)
divideBtn.addEventListener('click', divide)